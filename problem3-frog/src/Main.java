import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Integer[] array = {10, 30, 40, 20};
        Integer[] array1 = {30, 10, 60, 10, 60, 50};
        System.out.println(minimumCost(array.length-1, convertArrayToList(array)));
        System.out.println(minimumCost(array1.length-1, convertArrayToList(array1)));
    }

    public static Map<Integer, Integer> map = new HashMap<>();

    public static Integer minimumCost(Integer k, List<Integer> difference) {
        if (k == 0) {
            return 0;
        }

        Integer jumpOne = 0, jumpTwo = Integer.MAX_VALUE, min = 0;
        jumpOne = minimumCost(k-1, difference) + Math.abs(difference.get(k)-difference.get(k-1));
        if (k > 1) {
            jumpTwo = minimumCost(k-2, difference) + Math.abs(difference.get(k)-difference.get(k-2));
        }

        min = Math.min(jumpOne, jumpTwo);
        map.putIfAbsent(k, min);
        return min;
    }

    public static List<Integer> convertArrayToList(Integer[] array) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i<array.length; i++) {
            list.add(array[i]);
        }
        return list;
    }
}

//public class Main {
//    public static void main(String[] args) {
//        System.out.println(fibbonaci(5));
//        System.out.println(fibbonaci(6));
//        System.out.println(fibbonaci(7));
//        System.out.println(fibbonaci(50));
//    }
//
//    public static Long fibbonaci(Integer number) {
//        if (number <= 1) {
//            return (long) number;
//        }
//        Long A = (long) 0;
//        Long B = A + 1;
//        Long total = (long) 0;
//        for (Integer i = 2; i<=number; i++) {
//            total = A + B;
//            A = B;
//            B = total;
//        }
//        return B;
//    }
//}